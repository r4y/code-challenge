package com.ray.code.solutions;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public class Challenge2 {
	public static int fromHexadecimalToDecimal(String hexadecimalNumber) {
		int decimalConverted = 0;
		final int charAInHexadecimalToDec = 10;
		
		for(int i = hexadecimalNumber.length() - 1; i >= 0; i--) {
			int numToAdd = 0;
			
			if(hexadecimalNumber.charAt(i) >= '1' && hexadecimalNumber.charAt(i) <= '9') {
				numToAdd = Character.getNumericValue(hexadecimalNumber.charAt(i));
			}
			else if(hexadecimalNumber.charAt(i) >= 'a' && hexadecimalNumber.charAt(i) <= 'f') {
				numToAdd = (int)(hexadecimalNumber.charAt(i) - 'a') + charAInHexadecimalToDec;
			}
			
			decimalConverted += numToAdd * Math.pow(16, hexadecimalNumber.length() - 1 - i);
		}
		
		return decimalConverted;
	}
	
	public static void convertHexToDec(String file) {
		try {
			Path filePath = FileManager.fromStringLocationToPath(file);
			List<String> hexadecimalNumbers = FileManager.getDataLineByLine(filePath);
			StringBuilder outputToFile = new StringBuilder();
			
			hexadecimalNumbers.stream().forEach(hexadecimalNumber -> {
				outputToFile.append(fromHexadecimalToDecimal(hexadecimalNumber.trim()));
				outputToFile.append("\n");
			});
			outputToFile.deleteCharAt(outputToFile.length() - 1);
			
			FileManager.writeIntoFile(FileManager.fromStringLocationToPath("output-for-" + file), outputToFile.toString());
			//System.out.println(outputToFile.toString());
		} catch (IOException e) {
			System.out.println("Could not read file");
		}
		//System.out.println(fromHexadecimalToDecimal("10"));
	}
	public static void main(String[] args) {
		convertHexToDec("input-2.txt");
	}
}
