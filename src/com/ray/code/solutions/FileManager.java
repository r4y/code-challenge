package com.ray.code.solutions;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class FileManager {
	public static List<String> getDataLineByLine(Path filePath) throws IOException {
		return Files.readAllLines(filePath);
	}
	
	public static Path fromStringLocationToPath(String location) {
		return Paths.get(location);
	}
	
	public static void writeIntoFile(Path filePath, String textToWrite) throws IOException {
		Files.write(filePath, textToWrite.getBytes());
	}
	
}
