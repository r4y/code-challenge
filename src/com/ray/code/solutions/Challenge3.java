package com.ray.code.solutions;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class Challenge3 {
	public static String getHiddenMessage(List<String> messageLines) {
		StringBuilder messageBuilder = new StringBuilder(messageLines.size());
		int index = 1;
		do {
			String[] splittedLineMessage = messageLines.get(index - 1).split(":");
			int characterDecimal = Integer.parseInt(splittedLineMessage[2],16);
			//int characterDecimal = Challenge2.fromHexadecimalToDecimal(splittedLineMessage[2].toLowerCase()); //JUST FOR FUN
			index = Integer.valueOf(splittedLineMessage[1]);
			if(characterDecimal > 0) {
				messageBuilder.append((char) characterDecimal);
			}
		}while(index != 0);
		return messageBuilder.toString();
	}
	
	public static void getHiddenMessage(String file) {
		try {
			Path filePath = FileManager.fromStringLocationToPath(file);
			List<String> messageLines = FileManager.getDataLineByLine(filePath);
			List<String> messageLinesArray = new ArrayList<String>(messageLines);
			//System.out.println(getMessage(messageLinesArray));
			FileManager.writeIntoFile(FileManager.fromStringLocationToPath("output-for-" + file), getHiddenMessage(messageLinesArray));
		} catch (IOException e) {
			System.out.println("Could not read file");
		}
	}

	public static void main(String[] args) {
		getHiddenMessage("input-3.txt");
	}

}
