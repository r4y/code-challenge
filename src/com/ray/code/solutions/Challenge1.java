package com.ray.code.solutions;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Challenge1 {
	
	public static List<List<Integer>> getConsecutivesRanges(List<Integer> consecutiveNumbersIn){
		List<List<Integer>> consecutiveRangesList = new ArrayList<List<Integer>>(); 
		List<Integer> consecutiveRange = new ArrayList<Integer>(2);
		int consecutiveNumberLength = consecutiveNumbersIn.size();
		
		for(int i = 0; i < consecutiveNumberLength - 1; i++) {
			boolean isPairConsecutive = consecutiveNumbersIn.get(i + 1) - consecutiveNumbersIn.get(i) == 1;
			boolean isLastPair = i + 2 == consecutiveNumberLength;
			
			if(consecutiveRange.isEmpty()) {
				if(isPairConsecutive) {
					consecutiveRange.add(consecutiveNumbersIn.get(i));
					if(isLastPair) {
						consecutiveRange.add(consecutiveNumbersIn.get(i + 1));
						consecutiveRangesList.add(consecutiveRange);
					}
				}
			}
			else {
				if(!isPairConsecutive && !isLastPair) {
					consecutiveRange.add(consecutiveNumbersIn.get(i));
					consecutiveRangesList.add(consecutiveRange);
					consecutiveRange = new ArrayList<Integer>(2);
				}
				else if(isLastPair && isPairConsecutive) {
					consecutiveRange.add(consecutiveNumbersIn.get(i + 1));
					consecutiveRangesList.add(consecutiveRange);
					consecutiveRange = new ArrayList<Integer>(2);
				}
			}	
		}
		
		return consecutiveRangesList;
	}
	
	static void getConsecutiveRanges(String filePathIn) {
		try {
			Path filePath = FileManager.fromStringLocationToPath(filePathIn);
			List<String> consecutiveNumbersString = FileManager.getDataLineByLine(filePath);
			StringBuilder outputToFile = new StringBuilder();
			
			consecutiveNumbersString.stream().forEach(numbersList -> {
				List<String> numbersSplitted = Arrays.asList(numbersList.split(" "));
				List<Integer> integerArrays = numbersSplitted.stream()
						.map(n -> Integer.valueOf(n)).collect(Collectors.toList());
				List<List<Integer>> rangesList = getConsecutivesRanges(integerArrays);
				
				rangesList.stream().forEach(range -> {
					outputToFile.append(range.get(0));
					outputToFile.append("-");
					outputToFile.append(range.get(1));
					outputToFile.append(" ");
				});
				outputToFile.deleteCharAt(outputToFile.length() - 1);
				outputToFile.append("\n");
			});
			
			outputToFile.deleteCharAt(outputToFile.length() - 1);
			FileManager.writeIntoFile(FileManager.fromStringLocationToPath("output-for-" + filePathIn), outputToFile.toString());
			System.out.println(outputToFile.toString());
		} catch (IOException e) {
			System.out.println("Could not read file");
		}
		
		//List<Integer> consecutiveNumbersIn = new ArrayList<Integer>(Arrays.asList(new Integer[] {1,3,5,6,7,9,11,12,13}));
		//System.out.println(getConsecutivesRanges(consecutiveNumbersIn));
	}

	public static void main(String[] args) {
		getConsecutiveRanges("input-1.txt");
	}

}